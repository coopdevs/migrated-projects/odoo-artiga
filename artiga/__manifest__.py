{
    "name": "Odoo customizations for Artiga",
    "version": "12.0.1.0.0",
    "depends": ["l10n_es_pos"],
    "author": "Coopdevs Treball SCCL",
    "category": "Project Management",
    "website": "https://coopdevs.org",
    "license": "AGPL-3",
    "summary": """
        Odoo customizations for Artiga.
    """,
    "data": [],
    "qweb": ["static/src/xml/pos.xml"],
    "installable": True,
}
